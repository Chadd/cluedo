package SER215.Cluedo.ClueGameConsole;

import SER215.Cluedo.ClueGame.Game;
import SER215.Cluedo.ClueGame.GameUI;
import java.io.Console;

/**
 * A basic text interface for the Cluedo game. This UI creates a new
 * instance of the game object, registering itself as a callback
 * and waits for the Cluedo game to call this classes methods
 */
public class ConsoleUI implements GameUI {
	
	private Game activeGame;
	
	public static void main(String[] args) {

		// Create a new instance of the UI and run it. When/if it
		// returns, we assume the game has ended and let the JVM close
		ConsoleUI consoleUI = new ConsoleUI();
	}
	
	public ConsoleUI() {
		
		// Start a new game!
		activeGame = new Game(this);
		activeGame.play();
		
		// The game is now running and will call us for events as needed
		
	}
	
	/**
	 * Prompts the user to select either a yes or no response
	 * to the provided prompt.
	 * @param strQuestion The prompt to be used for the question
	 */
	public boolean askYesNo(String strQuestion) {
		boolean bolReturn = false;
		System.out.println(strQuestion);
		Console console = System.console();
		String inString = console.readLine("(Y)es or (N)o ?");
		inString = inString.toUpperCase();
		
		if (inString.matches("^[Y](ES)?")) {
			bolReturn = true;
		}
		
		return bolReturn;
	}
	
	/**
	 * Provide the interface with a set of options and then allow
	 * the user to select one
	 * @param strOptions an array of options to pick from
	 * #return the array index of the selected item
	 */
	public int pickOne(String strMessage, String[] strOptions) {
		int intReturn = -1;
		int intList = 1;
		System.out.println(strMessage);
		for(String anOption: strOptions){
			System.out.println(intList + " ) " + anOption);
			intList++;
		}
		
		// Get input
		Console console = System.console();
		String returnString = console.readLine("Please enter a selection between 1 and " + (intList - 1) + " ->  ");
		
		// Attempt to parse integer and revert to default if an error
		try {
			intReturn = Integer.parseInt(returnString);
		} catch (Exception e) {
			// Keep default -1 value and return that
		}
		
		return intReturn - 1;
	}
	
	/**
	 * Print the provided string to the UI
	 * @param strPrint The string to be printed
	 */
	public void print(String strPrint) {
		System.out.println(strPrint);
	}
	
	/**
	 * Prompt the user for a string
	 * @param strPrompt Text to use at the string entry prompt
	 */
	public String prompt(String strPrompt) {
		Console console = System.console();
		String returnString = console.readLine(strPrompt);
		return returnString;
	}
	
	/**
	 * Attempts to clear the UI of all content
	 */
	public void clear() {
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}
	
	/**
	 * Blocks until the user presses enter
	 */
	public void pause() {
		Console console = System.console();
		console.readLine("\nPress enter to continue... \n");		
	}	
	
	/**
	 * Asks the UI to terminate
	 */
	public void exit() {
		
		System.out.println("Thanks for playing Cluedo!");
		
	}
}
