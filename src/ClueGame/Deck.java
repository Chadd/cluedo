package SER215.Cluedo.ClueGame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {
	List<Card> cards;
	
	public static final String[] ROOMS = new String[] {"Kitchen", "Ballroom", "Conservatory", "Dining Room", "Billiard Room", "Library", "Lounge", "Hall", "Study"};
	public static final String[] WEAPONS = new String[] {"Candlestick", "Knife", "Lead pipe", "Revolver", "Rope", "Wrench"};
	public static final String[] SUSPECTS = new String[] {"Mrs. White", "Mr. Green", "Mrs. Peacock", "Professor Plum", "Miss Scarlet", "Colonel Mustard"};
	
	/**
	 * Creates a standard Cluedo deck containing 21 cards. The default
	 * constructor also shuffles the deck.
	 */
	public Deck() {
		
		cards = new ArrayList<Card>();
		
		for (String aCardName: ROOMS) {
			cards.add(new RoomCard(aCardName));
		}
		for (String aCardName: WEAPONS) {
			cards.add(new WeaponCard(aCardName));
		}
		for (String aCardName: SUSPECTS) {
			cards.add(new SuspectCard(aCardName));
		}
		shuffle();

	}
	
	/**
	 * Uses the built in java shuffle function to shuffle the elements
	 * in the card ArrayList
	 */
	private void shuffle() {

		// Shuffle the cards in the deck
		Collections.shuffle(cards);
	}
	
	/**
	 * Pops the last card in the deck
	 * @return The last card in the deck
	 */
	public Card pop() {
		Card popCard;
		if (!cards.isEmpty()) {
			popCard = cards.get(cards.size() - 1);
			cards.remove(cards.size() - 1);
		} else {
			popCard = new Card();
		}
		return popCard;
	}
	
	/**
	 * Pop a card of the specified type. This function is especially
	 * handy for popping cards for the secret
	 * @param cardType the Class specification for the desired card type
	 * 			EXAMPLE; SuspectCard.getClass() as the argument
	 */
	public Card pop(Class cardType) {
		Card returnCard = null;
		for(Card card: cards){
			if (card.getClass().equals(cardType)) {
				returnCard = card;
			}
		}
		
		// Remove found card (if one was found)
		if (returnCard != null) {
			cards.remove(returnCard);
		}
		
		return returnCard;
	}
	
	/**
	 * Returns last card in the list without removing it from the deck
	 * @return The last card in the deck
	 */
	public Card peek() {
		Card peekCard;
		if (!cards.isEmpty()) {
			peekCard = cards.get(cards.size() - 1);
		} else {
			peekCard = new Card();
		}
		return peekCard;
	}
	
	public boolean isEmpty() {
		return cards.isEmpty();
	}
	public String toString() {
		StringBuilder outString = new StringBuilder();
		
        for(Card card: cards){
            outString.append(card.toString());
            outString.append('\n');
        }
        
        return outString   .toString();     
	}
}
