package SER215.Cluedo.ClueGame;

public class Guess {
	public String suspect;
	public String room;
	public String weapon;
	
	public String incorrectComponent;
	
	public Guess() {
		suspect = "";
		room = "";
		weapon = "";
		
		incorrectComponent = "";
	}
	
	public Guess(String aSuspect, String aRoom, String aWeapon) {
		room = aRoom;
		weapon = aWeapon;
		suspect = aSuspect;
		incorrectComponent = "";
	}
	
	public boolean equals(Guess aGuess) {
		boolean bolEqual = false;
		if (this.room.equals(aGuess.room) &&
			this.weapon.equals(aGuess.weapon) &&
			this.suspect.equals(aGuess.suspect) ) {
			
			bolEqual = true;
		}
		return bolEqual;
	}
	
	public String toString() {
		StringBuilder returnString = new StringBuilder();
		returnString.append(suspect);
		returnString.append(" in the ");
		returnString.append(room);
		returnString.append(" with the ");
		returnString.append(weapon);
		return returnString.toString();		
	}
}
