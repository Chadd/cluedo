package SER215.Cluedo.ClueGame;

public class Card {
	
	private String name;
	
	public Card() {
		name = "";
	}
	
	public Card(String cardName) {
		this.name = cardName;
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return name + " :: " + this.getClass().getName();
	}
}
