package SER215.Cluedo.ClueGame;

public class Solution {
	
	private WeaponCard weapon;
	private RoomCard room;
	private SuspectCard suspect;
	
	public Solution(SuspectCard aSuspect, RoomCard aRoom, WeaponCard aWeapon) {
		suspect = aSuspect;
		room = aRoom;
		weapon = aWeapon;
	}
	
	public boolean equals(Guess aGuess) {
			boolean bolEqual = false;
		if (this.room.getName().equals(aGuess.room) &&
			this.weapon.getName().equals(aGuess.weapon) &&
			this.suspect.getName().equals(aGuess.suspect) ) {
			
			bolEqual = true;
		}
		return bolEqual;
	}
	
	public String toString() {
		StringBuilder outString = new StringBuilder();
		outString.append("Solution says it was; ");
		outString.append(suspect.getName());
		outString.append(" with the ");
		outString.append(weapon.getName());
		outString.append(" in the ");
		outString.append(room.getName());
		outString.append('\n');
		return outString.toString();
	}
}

