package SER215.Cluedo.ClueGame;

public class HumanPlayer extends Player {
	
	public HumanPlayer() {
		super();
	}
	
	public HumanPlayer(String strName) {
		super(strName);
	}
	
	public void giveCard(Card aCard) {
		this.cards.add(aCard);
	}
	
	public Guess makeGuess(GameUI callableUI) {
		Guess returnGuess = new Guess();
		
		callableUI.clear();
		StringBuilder strInfo = new StringBuilder();
		
		strInfo.append("Your cards\n==========\n");
		for (Card aCard: cards) {
			strInfo.append(aCard.getName());
			strInfo.append('\n');
		}
		
		strInfo.append("\nYour guesses so far...\n=======================\n");
		for (Guess aGuess: guesses) {
			strInfo.append(aGuess.toString());
			strInfo.append(" but ");
			strInfo.append(aGuess.incorrectComponent);
			strInfo.append(" was disproven\n");
		}
		callableUI.print(strInfo.toString());
		int idSuspect = callableUI.pickOne("Please select a suspect", Deck.SUSPECTS);
		int idRoom = callableUI.pickOne("Please select a room", Deck.ROOMS);
		int idWeapon = callableUI.pickOne("Please select a weapon", Deck.WEAPONS);
		
		returnGuess = new Guess(Deck.SUSPECTS[idSuspect], Deck.ROOMS[idRoom], Deck.WEAPONS[idWeapon]);
		return returnGuess;
	}
}
