package SER215.Cluedo.ClueGame;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

abstract class Player {
	
	protected String name;
	protected List<Guess> guesses;
	protected List<Card> cards;
	
	public Player() {
		guesses = new ArrayList<Guess>();
		cards = new ArrayList<Card>();
	}
	
	public Player(String strName) {
		guesses = new ArrayList<Guess>();
		cards = new ArrayList<Card>();
		this.name = strName;
	}
	
	public String getName() {
		return name;
	}
	
	public abstract void giveCard(Card aCard);
	
	public void addGuess(Guess aGuess) {
		guesses.add(aGuess);
	}
	public boolean hasCard(Card aCard) {
		return cards.contains(aCard);
	}

	public boolean hasCard(String cardName) {
		boolean bolReturn = false;
		for (Card aCard: cards) {
			if (cardName.equals(aCard.getName())) {
				bolReturn = true;
			}			
		}
		return bolReturn;
	}
	
	public abstract Guess makeGuess(GameUI callableUI);
	
}
