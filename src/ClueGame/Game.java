package SER215.Cluedo.ClueGame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The core implementation of the Cluedo game. This class handles the
 * game logic loop and alerts the GameUI with events.
 */
public class Game {
		
	private GameUI localGameUI;
	List<Player> players;
	private Deck deck;
	private Solution solution;
	
	public Game(GameUI aGameUI) {
		
		// Register the provided UI locally so we can call it later
		localGameUI = aGameUI;
		
		// Deck init
		deck = new Deck();
		players = new ArrayList<Player>();
		
	}
	
	/**
	 * Main game logic loop
	 */
	public boolean play() {
		
		// Welcome player and ask them to select a character
		int playerIndex = 0;
		localGameUI.print("Welcome to Cluedo!");
		playerIndex = localGameUI.pickOne("Please select a character", Deck.SUSPECTS);
		players.add(new HumanPlayer(Deck.SUSPECTS[playerIndex]));
		localGameUI.clear();
		localGameUI.print("Welcome, " + players.get(0).getName());
		
		// Determine the number of AI players
		int aiPlayers = 1;
		aiPlayers = localGameUI.pickOne("How many opponents would you like?", new String[] {"1", "2", "3", "4", "5"});
		int intChooser = 0;
		while (players.size() < aiPlayers + 2) {
			if (playerIndex == intChooser) {
				//Don't add this one because it has already been selected
			} else {
				players.add(new RobotPlayer(Deck.SUSPECTS[intChooser]));
			}
			intChooser++;
		}
		
		// Pop the solution cards from the deck
		SuspectCard suspect = new SuspectCard();
		suspect = (SuspectCard) deck.pop(suspect.getClass());
		RoomCard room = new RoomCard();
		room = (RoomCard) deck.pop(room.getClass());
		WeaponCard weapon = new WeaponCard();
		weapon = (WeaponCard) deck.pop(weapon.getClass());
		solution = new Solution(suspect, room, weapon);
		System.out.println(solution.toString());
		// Deal cards to each player
		int nextCardTo = 0;
		while (!deck.isEmpty()) {
			
			// Check to see if the last card was dealt to the last player in the list
			if (nextCardTo > players.size() - 1) {
				nextCardTo = 0;
			}
			
			// Deal card according to next player itterator
			players.get(nextCardTo).giveCard(deck.pop());
			nextCardTo++;
		}
		
		// Main game loop
		boolean runGame = true;
		int currentRound = 1;
		int currentPlayer = 0;
		while (runGame) {
			localGameUI.print("\nRound " + currentRound + " \n ========");
			for (Player aPlayer: players) {
				Guess aGuess = aPlayer.makeGuess(localGameUI);
				
				// If the guess does not match the secret, we need to determine which player has a disputing card
				if (!solution.equals(aGuess)) {
					
					// Determine which card disproves this guess
					String cardOwner = "";
					String cardName = "";
					
					for (Player checkPlayer : players) {
						
						// Check room card
						if (checkPlayer.hasCard(aGuess.room)) {
							cardOwner = checkPlayer.getName();
							cardName = aGuess.room;
							aGuess.incorrectComponent = cardName;
						}
						
						// Check weapon
						if (checkPlayer.hasCard(aGuess.weapon)) {
							cardOwner = checkPlayer.getName();
							cardName = aGuess.weapon;
							aGuess.incorrectComponent = cardName;
						}
						
						// Check suspect
						if (checkPlayer.hasCard(aGuess.suspect)) {
							cardOwner = checkPlayer.getName();
							cardName = aGuess.suspect;
							aGuess.incorrectComponent = cardName;
						}					
					}
					
					// if an incorrect component was added to aGuess, we need to post that to the UI
					StringBuilder strMessage = new StringBuilder();
					strMessage.append(aPlayer.getName());
					strMessage.append(" makes a guess of; ");
					strMessage.append(aGuess.suspect);
					strMessage.append(" in the ");
					strMessage.append(aGuess.room);
					strMessage.append(" with the ");
					strMessage.append(aGuess.weapon);
					strMessage.append('\n');
					
					if (aPlayer instanceof HumanPlayer) {
						strMessage.append(cardOwner);
						strMessage.append(" shows you their card of; ");
						strMessage.append(cardName);
						strMessage.append(" which disproves your guess \n");
					} else {
						strMessage.append(cardOwner);
						strMessage.append(" secretly shows ");
						strMessage.append(aPlayer.getName());
						strMessage.append(" a card that disproves their guess \n");
					}
					
					aPlayer.addGuess(aGuess);
					localGameUI.print(strMessage.toString());
					localGameUI.pause();
				} else {
					StringBuilder strMessage = new StringBuilder();
					strMessage.append("WOW! ");
					strMessage.append(aPlayer.getName());
					strMessage.append(" has solved the mystery!\n");
					strMessage.append("The correctly guessed solution was;\n");
					strMessage.append(aGuess.suspect);
					strMessage.append(" in the ");
					strMessage.append(aGuess.room);
					strMessage.append(" with the ");
					strMessage.append(aGuess.weapon);
					strMessage.append('\n');
					
					runGame = false;
					localGameUI.clear();
					localGameUI.print(strMessage.toString());
					if (localGameUI.askYesNo("Would you like to play again?")) {
						return true;
					} else {
						return false;
					}
				}
				
				
			}
		}
		
		// Exit when done
		localGameUI.exit();
		return false;
	}	
}
