package SER215.Cluedo.ClueGame;

/**
 * A baseline interface for the Game class to call with events
 */
public interface GameUI {
	
	/**
	 * Prompts the user to select either a yes or no response
	 * to the provided prompt.
	 * @param strQuestion The prompt to be used for the question
	 */
	boolean askYesNo(String strQuestion);
	
	/**
	 * Provide the interface with a set of options and then allow
	 * the user to select one
	 * @param strOptions an array of options to pick from
	 * @param strMessage A message to print explaining the input request
	 * @return the array index of the selected item
	 */
	int pickOne(String strMessage, String[] strOptions);
	
	/**
	 * Print the provided string to the UI
	 * @param strPrint The string to be printed
	 */
	void print(String strPrint);
	
	/**
	 * Prompt the user for a string
	 * @param strPrompt Text to use at the string entry prompt
	 */
	String prompt(String strPrompt);
	
	/**
	 * Attempts to clear the UI of all content
	 */
	void clear();
	
	/**
	 * Blocks until the user presses enter
	 */
	void pause();
	
	/**
	 * Asks the UI to terminate
	 */
	void exit();
}
