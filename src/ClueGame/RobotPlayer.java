package SER215.Cluedo.ClueGame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Arrays;

public class RobotPlayer extends Player {
	
	private List<String> possibleRooms;
	private List<String> possibleWeapons;
	private List<String> possibleSuspects;
	
	public RobotPlayer() {
		super();
		this.setup();
	}
	
	public RobotPlayer(String strName) {
		super(strName);
		this.setup();
	}
	
	
	public void giveCard(Card aCard) {
		if (!this.cards.contains(aCard)) {
			this.cards.add(aCard);
		}
		
		if (aCard instanceof RoomCard) {
			possibleRooms.remove(aCard.getName());
		} else if (aCard instanceof WeaponCard) {
			possibleWeapons.remove(aCard.getName());
		} else if (aCard instanceof SuspectCard) {
			possibleSuspects.remove(aCard.getName());
		}
	}
	
	public Guess makeGuess(GameUI callableUI) {
		Guess returnGuess = new Guess();
		shufflePossible();
		
		returnGuess.room = possibleRooms.get(0);
		returnGuess.weapon = possibleWeapons.get(0);
		returnGuess.suspect = possibleSuspects.get(0);
		
		return returnGuess;
	}
	
	/*
	 * A shared setup sequence for the parts unique to the AI player
	 */
	private void setup() {
		this.possibleRooms = new ArrayList<String>(Arrays.asList(Deck.ROOMS));
		this.possibleWeapons = new ArrayList<String>(Arrays.asList(Deck.WEAPONS));
		this.possibleSuspects = new ArrayList<String>(Arrays.asList(Deck.SUSPECTS));	
	}	
	
	private void shufflePossible() {
		Collections.shuffle(possibleRooms);
		Collections.shuffle(possibleWeapons);
		Collections.shuffle(possibleSuspects);
	}
	
	/**
	 * Checks to see if the given element exists in the local guesses
	 * database. Specifically, this checks to see if the element
	 * has been disproven. This is used in the AI routing so the AI
	 * does not pick the same incorrect element more than once.
	 */
	private boolean elementIncorrect(String strElement) {
		boolean bolReturn = false;
		for (Guess aGuess: this.guesses) {
			if (strElement.equals(aGuess.incorrectComponent)) {
				bolReturn = true;
			}
		}
		return bolReturn;
	}
	
}
